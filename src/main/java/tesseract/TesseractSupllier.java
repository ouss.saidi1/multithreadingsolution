package tesseract;

import java.util.function.Supplier;

import net.sourceforge.tess4j.Tesseract;

/**
 * TesseractSupllier
 */
public class TesseractSupllier implements Supplier<MultiThreadTesseract>{
    private String tessDataPath;
    private String lang;

    public TesseractSupllier(String tessDataPath,String lang) {
        super();
        this.tessDataPath = tessDataPath;
        this.lang = lang;
    }
    @Override
    public MultiThreadTesseract get() {
        Tesseract tesseract = new Tesseract();
        tesseract.setDatapath(tessDataPath);
        tesseract.setLanguage(lang);
        return new MultiThreadTesseract(tesseract);
    }
    
}