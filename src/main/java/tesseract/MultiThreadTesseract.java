package tesseract;

import java.io.File;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import ocp.abstraction.MultiThreadWorker;
 
public class MultiThreadTesseract extends MultiThreadWorker<File,String> {
    private Tesseract tesseract ;

    public MultiThreadTesseract(Tesseract tesseract) {
        this.tesseract = tesseract;
        }


    @Override
    public String apply(File file) {
        try {
            return tesseract.doOCR(file);
        } catch (TesseractException e) {
            throw new RuntimeException(e);
        }
    }
}
