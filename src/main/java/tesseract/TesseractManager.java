package tesseract;

import java.util.function.Supplier;

import ocp.abstraction.WorkerManager;
import ocp.constant.WorkerStats;

/**
 * TesseractManager
 */
public class TesseractManager extends WorkerManager<MultiThreadTesseract> {

    public TesseractManager(Supplier<MultiThreadTesseract> supplier, int numberOfInstances) {
        super(supplier, numberOfInstances);
    }

    @Override
    public MultiThreadTesseract getNextAvailable() {
        return this.getInstanceList().stream().filter(worker -> WorkerStats.AVAILABLE.equals(worker.getState())).findAny().orElse(null);
    }

    
}