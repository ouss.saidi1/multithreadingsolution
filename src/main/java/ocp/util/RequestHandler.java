package ocp.util;

import java.util.concurrent.Callable;

import ocp.abstraction.MultiThreadWorker;

public class RequestHandler<Request,Response> implements Callable<Response> {

    private MultiThreadWorker<Request,Response> multiThreadWorker;
    private Request request;
    public  RequestHandler(Request request , MultiThreadWorker<Request,Response> multiThreadWorker){
        this.request = request;
        this.multiThreadWorker = multiThreadWorker;
    }    
    @Override
    public Response call() throws Exception {
        System.out.println("worked on by instance id : " + multiThreadWorker.getId());
        Response response = multiThreadWorker.apply(request);
        System.out.println("worker whit id : " + multiThreadWorker.getId()  + " in  now available" );
        return response;
    }
    
}
