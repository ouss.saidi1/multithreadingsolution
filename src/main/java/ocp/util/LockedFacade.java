package ocp.util;

import java.io.File;

import ocp.abstraction.AbstractionFacade;
import ocp.abstraction.WorkerManager;


public class LockedFacade extends AbstractionFacade<File,String>{ 
        public LockedFacade(WorkerManager workerManager, int numberOfThreads) {
        super(workerManager,numberOfThreads);
    }
}
