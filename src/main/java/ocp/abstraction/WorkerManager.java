package ocp.abstraction;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;



/**
 * InstanceCreator
 */
public abstract class WorkerManager<T extends MultiThreadWorker> {
    private List<T>  instanceList;

    public WorkerManager(Supplier<T>  supplier, int numberOfInstances){   
        instanceList = new ArrayList<>();
        for (int i = 0; i < numberOfInstances; i++) {
            instanceList.add(supplier.get());
        }      
    }

    public List<T> getInstanceList() {
        return instanceList;
    }
    public abstract  T getNextAvailable();

}