package ocp.abstraction;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ocp.constant.WorkerStats;
import ocp.exception.ExceptionMessage;
import ocp.util.RequestHandler;

public abstract class AbstractionFacade<Request,Response> {
    private WorkerManager workerManager;
    private ExecutorService service;
    private static Lock lock = new ReentrantLock();

    public AbstractionFacade(WorkerManager workerManager,int numberOfThreads) {
        this.workerManager = workerManager;
        this.service = Executors.newFixedThreadPool(numberOfThreads);
    }

    public  Response process(Request request){
        MultiThreadWorker multiThreadWorker = getNextAvailable();
        RequestHandler<Request,Response> requestHandler = new RequestHandler<Request,Response>(request, multiThreadWorker);
        Future<Response> future = service.submit(requestHandler);
        Response res = null;
        try {
            System.out.println(multiThreadWorker.getId() + " is waiting for result");
            res = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        multiThreadWorker.setState(WorkerStats.AVAILABLE);
        return res;
    }

    private MultiThreadWorker getNextAvailable() {
        try {
            lock.lock();
            MultiThreadWorker worker = this.workerManager.getNextAvailable();
            while (Objects.isNull(worker)) {
                System.out.println("Waiting for Worker");
                try {
                    Thread.sleep(1000);
                    worker = this.workerManager.getNextAvailable();
                } catch (InterruptedException e) {
                    System.out.println(ExceptionMessage.SOMETHING_WENT_WRONG);
                    Thread.currentThread().interrupt();
                }
            }
            worker.setState(WorkerStats.NOT_AVAILABLE);
            return worker;
        } finally {
            lock.unlock();
        }
    }

        public ExecutorService getService() {
            return service;
        }
    @Override
    protected void finalize() throws Throwable {
        if(service!= null && !service.isShutdown())
            service.shutdown();
        super.finalize();
    }
}