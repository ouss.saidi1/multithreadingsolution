package ocp.abstraction;

import java.util.function.Function;

import ocp.constant.WorkerStats;


public abstract class MultiThreadWorker<Request,Response> implements Function<Request,Response> {
    private volatile static int numberOfInstance = 0;
    private int id ;
    protected WorkerStats state;
    public MultiThreadWorker(){
        id = numberOfInstance;
        numberOfInstance = numberOfInstance + 1;
        this.state = WorkerStats.AVAILABLE;
    }
    public WorkerStats getState() {
        return state;
    }
    public void setState(WorkerStats state) {
        this.state = state;
    }
    public int getId() {
        return id;
    }
    @Override
    public String toString() {
        
        return "id = " + this.id + " stats is = "+ this.state;
    }
}
