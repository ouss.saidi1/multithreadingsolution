package ocp.exception;

/**
 * NoWorkerIsAvailabeException
 */
public class NoWorkerIsAvailabeException extends Exception {
    public NoWorkerIsAvailabeException(String message){
        super(message);
    }
    
}