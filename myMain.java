import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class myMain {
    static File file = new File("C:\\Users\\DELL\\Desktop\\MyProject\\multithreadingsolution\\com\\ressources\\images\\test.png");
    static String data = "C:\\Users\\DELL\\Desktop\\MyProject\\multithreadingsolution\\com\\ressources\\Tess4J";

    static List<Integer> list = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args){
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        Thread thread = new Thread(() -> {
            list.remove(new Integer(5));
        });

        Thread thread1 = new Thread(() -> {
            list.remove(new Integer(6));
        });

        thread.start();
        thread1.start();
        while(list.contains(5) || list.contains(6) ){
            System.out.println("Waiting");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("fin");
    }


}